FROM nginx:1.13-alpine
RUN apk add --update tzdata && \
    cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && \
    apk del tzdata
COPY ./default.conf /etc/nginx/conf.d/default.conf
COPY ./reactjs-cicd/build /usr/share/nginx/html
WORKDIR /usr/share/nginx/html